package malen;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.BorderFactory;

public class PKreis extends javax.swing.JPanel {

    public PKreis() {
        initComponents();
        setBorder(BorderFactory.createLineBorder(Color.black));
    }

     public void paintComponent(Graphics g) {
        super.paintComponent(g);       

        g.setColor(Color.yellow);
        g.fillRect(10, 10, getWidth()-20, getHeight()-20);
        
        // Draw Text
        g.setColor(Color.black);
        g.drawString("This is my custom Panel!", 20, 20);
    }  
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setPreferredSize(new java.awt.Dimension(200, 200));

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
