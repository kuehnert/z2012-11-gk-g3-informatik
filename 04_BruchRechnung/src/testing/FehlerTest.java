package testing;

import bruchrechnung.Bruch;

public class FehlerTest {
    public static void testeExceptions() {
        int[] a = {};
        
        Bruch c = new Bruch(5, 6);
        c = c.erweitere(3);
        System.out.println("Bruch: " + c);

        try {
            c = c.erweitere(-3);
            System.out.println("Bruch: " + c);
        } catch (Exception e) {
            System.out.println("Es ist ein Fehler aufgetreten.");
        }
        
        try {
            int b = 5 / 0;
            System.out.println("b = " + b);
        } catch (ArithmeticException e) {
            System.out.println("Du dummer Benutzer, man darf nicht durch 0 teilen.");
        }
        
        // System.out.println( a[1] );
        System.out.println("Das Programm endet ordnungsgemaess.");
    }

    public static void main(String[] args) {
        Bruch b = new Bruch(0, 1);
        b = b.kehrwert();
        System.out.println("Bruch: " + b);
    }
}
