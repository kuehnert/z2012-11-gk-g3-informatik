package bruchrechnung;

public class Bruch {
    private int zaehler, nenner;
    
    public Bruch(int neuerZaehler, int neuerNenner) throws ArithmeticException {
        setzeZaehler(neuerZaehler);
        setzeNenner(neuerNenner);
    }
    
    public Bruch(int ganzzahl) {
        zaehler = ganzzahl;
        nenner  = 1;    
    }

    public void setzeNenner(int neuerNenner) {
        if (neuerNenner == 0) {
            throw new ArithmeticException("Nenner darf nicht 0 sein.");
        }

        nenner = neuerNenner;
    }
    
    public int gibZaehler() {
        return zaehler;
    }
    
    public void setzeZaehler(int neuerZaehler) {
        zaehler = neuerZaehler;
    }

    public Bruch multipliziere(int faktor) {
        return new Bruch(zaehler * faktor, nenner);
    }

    public Bruch multipliziere(Bruch zweiterBruch) {
        //  a     c    a*c
        // --- * --- = ---
        //  b     d    b*d
        int neuerZaehler = zaehler * zweiterBruch.gibZaehler();
        int neuerNenner  = nenner  * zweiterBruch.gibNenner();
        
        return new Bruch(neuerZaehler, neuerNenner);
    }

    public int gibNenner() {
        return nenner;
    }
    
    public Bruch kehrwert() {
        int neuerZaehler = nenner * Integer.signum(zaehler);
        int neuerNenner  = Math.abs(zaehler);
        return new Bruch(neuerZaehler, neuerNenner);
    }
    
    public Bruch dividiere(Bruch andererBruch) {
        return multipliziere( andererBruch.kehrwert() );
    }
    
    public Bruch dividiere(int divisor) {
        return new Bruch(zaehler, nenner * divisor);
    }
    
    public Bruch erweitere(int faktor) throws ArithmeticException {
        if (faktor <= 0) {
            throw new ArithmeticException("Faktor muss positiv sein!");
        } else {
            return new Bruch(zaehler * faktor, nenner * faktor);
        }
    }
    
    public String toString() {
        return zaehler + "/" + nenner;
    }
        
    public Bruch addiere(Bruch zweiterBruch) {
        int kgV = Mathe.kgV(nenner, zweiterBruch.gibNenner());
        int z1 = zaehler * kgV / nenner;
        int z2 = zweiterBruch.gibZaehler() * kgV / zweiterBruch.gibNenner();
        
        return new Bruch(z1 + z2, kgV);
    }
    
    public Bruch subtrahiere(Bruch zweiterBruch) {
        int kgV = Mathe.kgV(nenner, zweiterBruch.gibNenner());
        int z1 = zaehler * kgV / nenner;
        int z2 = zweiterBruch.gibZaehler() * kgV / zweiterBruch.gibNenner();
        
        return new Bruch(z1 - z2, kgV);
    }
    
    public double rational() {
        return (double) zaehler / nenner;
    }
}
