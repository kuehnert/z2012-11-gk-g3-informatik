package bruchrechnung;

public class Mathe {
    public static int ggT(int a, int b) {
        a = Math.abs(a);
        b = Math.abs(b);
        
        while (a != b) {
            if (b > a) {
                b = b - a;
            } else {
                a = a - b;
            }
        }

        return a;
    }

    public static int kgV(int a, int b) {
       a = Math.abs(a);
       b = Math.abs(b);
        
       return a * b / ggT(a, b);
    }
}
