package bruchrechnung;

import java.awt.Color;
import java.awt.Graphics;

public class BruchViewGraph extends javax.swing.JPanel {
    private Bruch bruch;

    public BruchViewGraph() {
        initComponents();
        setBruch( new Bruch(1, 2) );
    }
    
    public Bruch getBruch() {
        return bruch;
    }

    public void setBruch(Bruch bruch) {
        this.bruch = bruch;
        // Male Panel neu, weil sich Werte verändert haben
        repaint();
    }

    /*
     * Überschreiben:
     * Eine geerbte Methode neu definieren
     * Erweitern:
     * Ein Überschreiben, wo erst die Befehle der geerbten Metode ausgeführt
     * werden. Passiert mithilfe des Schlüsselworts "super"
     * Normalerweise steht der Aufruf von super am Anfang der überschreibenden
     * Methode
     */
    @Override
    protected void paintComponent(Graphics grphcs) {
        super.paintComponent(grphcs);
        
        grphcs.setColor(Color.yellow);
        // int hoehe = 210 * 0.75; // 157.5
        // int hoehe = (int) (getHeight() * bruch.rational());
        int hoehe = (int) Math.round(getHeight() * bruch.rational());
        grphcs.fillRect(0, 0, getWidth(), hoehe);
        
        // Text von Zähler und Nenner
        grphcs.setColor(Color.black);
        grphcs.drawString( "" + bruch.gibZaehler() , 50, 50);
        grphcs.drawString( "" + bruch.gibNenner() , 50, 80);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
