package logic;

public class OneTimePad {

    public static boolean[] generatePad(int n) {
        // Standardwert (default value) ist false
        boolean[] pad = new boolean[n];

        for (int i = 0; i < n; i++) {
            if (Math.random() < 0.5) {
                pad[i] = true;
            }
        }

        return pad;
    }

    public static boolean[] encode(boolean[] answers, boolean[] pad) {
        boolean[] chiffre = new boolean[answers.length];
        
        for (int i = 0; i < answers.length; i++) {
            if (pad[i] == true) {
                chiffre[i] = ! answers[i];
//                if (answers[i] == false) {
//                    answers[i] = true;
//                } else {
//                    answers[i] = false;
//                }
            }
        }

        return answers;
    }
}
