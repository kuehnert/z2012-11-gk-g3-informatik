package test;

import java.util.Arrays;
import logic.OneTimePad;

public class TestOneTimePad {
    public static void main(String[] args) {
        System.out.println( Arrays.toString( OneTimePad.generatePad(10) ));
    }
}
