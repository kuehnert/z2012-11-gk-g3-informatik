package crypt;

import java.util.Arrays;

public class Zahlen {
    public static String code(String source, String key) {
        String output = "";
        
        for (int i = 0; i < source.length(); i++) {
            int position = (int) source.charAt(i);
            output = output + position + " ";
        }
        
        return output;
    }
    
    public static String decode(String source, String key) {
        String[] zahlen = source.split(" ");
        String out = "";
        
        for (int i = 0; i < zahlen.length; i++) {
            out = out + (char) Integer.parseInt( zahlen[i] );
        }
        
        return out;
    }
}
