package crypt;

public interface Verfahren {
    public String code(String source, String key);

    public String decode(String source, String key);
}
