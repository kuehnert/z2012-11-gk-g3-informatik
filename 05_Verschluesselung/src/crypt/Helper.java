package crypt;

public class Helper {
   static final String[] ZIFFERN = { "null", "eins", "zwei", "drei", "vier",
       "fuenf", "sechs", "sieben", "acht", "neun" };

   static int zeichenPosition(String s, int i) {
       return s.charAt(i) - 'a';
   }

   static int zeichenPosition(String s) {
       return zeichenPosition(s, 0);
   }
   
   static String positionZeichen(int i) {
       return "" + (char) (i + 'a');
   }

    public static String cleanString(String in) {
        String output = "";
        in = in.toLowerCase();
        
        for (int i = 0; i < in.length(); i++) {
            if ( in.charAt(i) >= 'a' && in.charAt(i) <= 'z' ) {
                output = output + in.charAt(i);
            } else if ( in.charAt(i) >= '0' && in.charAt(i) <= '9' ) {
                output = output + zahlToString( in.charAt(i) - '0' );
            } else if ( in.charAt(i) == 'ä' ) {
                output = output + "ae";
            } else if ( in.charAt(i) == 'ö' ) {
                output = output + "oe";
            } else if ( in.charAt(i) == 'ü' ) {
                output = output + "ue";
            } else if ( in.charAt(i) == 'ß' ) {
                output = output + "ss";
            }
        }
        
        return output;
    }
    public static String zahlToString(int in) {
        if (in < 10) {
            return ZIFFERN[in];
        } else {
            return "kennichnich";
        }
    }
}
