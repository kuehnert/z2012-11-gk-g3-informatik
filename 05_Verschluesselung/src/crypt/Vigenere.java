package crypt;

public class Vigenere {
    public static String code(String source, String key) {
        
        String output = "";
        
        for (int i = 0; i < source.length(); i++) {
            int verschluesselungsindex = i % key.length();
            String verschluesselungszeichen = "" + key.charAt( verschluesselungsindex );
            int verschiebung = Helper.zeichenPosition( verschluesselungszeichen );
            
            // Position im Alphabet, von 0-25
            int position = Helper.zeichenPosition(source, i);
            int neuePosition = position + verschiebung;
            if (neuePosition > 25) {
                neuePosition = neuePosition - 26;
            }

            output = output + Helper.positionZeichen(neuePosition);
        }
        
        return output;
    }
    
    public static String decode(String source, String key) {
        // key-Zeichenketter in Integer-Position umwandeln (C => 2)
        int position = Helper.zeichenPosition(key);
        
        // Differenz berechnen (5 - 2 => 3)
        // Differenz in Zeichen umwandeln (3 => D)
        return code( source, Helper.positionZeichen(26 - position) );
    }
    
}
