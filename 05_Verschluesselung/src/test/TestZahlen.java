package test;

import crypt.Zahlen;

public class TestZahlen {
    public static void testCode() {
        String in = "abcde";
        String expected = "97 98 99 100 101 ";
        String result = Zahlen.code(in, "");
        
        if (result.equals(expected)) {
            System.out.println("Alles OK.");
        } else {
            System.out.println("Fehler: " + result + " != " + expected);
        }

        in = "xyz";
        expected = "120 121 122 ";
        result = Zahlen.code(in, "");
        
        if (result.equals(expected)) {
            System.out.println("Alles OK.");
        } else {
            System.out.println("Fehler: " + result + " != " + expected);
        }
}
    
    public static void testDecode() {
        String in = "97 98 99 100 101 ";
        String expected = "abcde";
        String result = Zahlen.decode(in, "egal");
        
        if (result.equals(expected)) {
            System.out.println("Alles OK.");
        } else {
            System.out.println("Fehler: " + result + " != " + expected);
        }

        in = "120 121 122 ";
        expected = "xyz";
        result = Zahlen.decode(in, "nicht verwendet");
        
        if (result.equals(expected)) {
            System.out.println("Alles OK.");
        } else {
            System.out.println("Fehler: " + result + " != " + expected);
        }
    }
    
    public static void main(String[] args) {
        testCode();
        testDecode();
    }
}
