package test;

import crypt.Helper;

public class TestHelper {
    public static void testCleanString() {
        String in = "Fuchs, Du hast die heißen Gänse um 15:04 gestohlen!";
        String expected = "fuchsduhastdieheissengaenseumeinsfuenfnullviergestohlen";
        String result = Helper.cleanString(in);
        
        if (result.equals(expected)) {
            System.out.println("Alles OK.");
        } else {
            System.out.println("Fehler: " + result + " != " + expected);
        }
    }
    
    public static void testZahlToString() {
        int in = 1;
        String expected = "eins";
        String result = Helper.zahlToString(in);
        
        if (result.equals(expected)) {
            System.out.println("Alles OK.");
        } else {
            System.out.println("Fehler: " + result + " != " + expected);
        }
        
        in = 2;
        expected = "zwei";
        result = Helper.zahlToString(in);
        
        if (result.equals(expected)) {
            System.out.println("Alles OK.");
        } else {
            System.out.println("Fehler: " + result + " != " + expected);
        }
        
        in = 3;
        expected = "drei";
        result = Helper.zahlToString(in);
        
        if (result.equals(expected)) {
            System.out.println("Alles OK.");
        } else {
            System.out.println("Fehler: " + result + " != " + expected);
        }
        
    }
    

    public static void main(String[] args) {
        testCleanString();
        testZahlToString();
    }
}

