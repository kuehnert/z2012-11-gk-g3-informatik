package test;

import crypt.Vigenere;

public class TestVigenere {
    public static void testCode() {
        String in = "aaaneverkleinernabcdefghijklmnopqrstuvewxyz";
        String expected = "dogqsbhfqosoqsxqohfrkiunlxqoatrdwugzxjkzlec";
        String result = Vigenere.code(in, "dog");
        
        if (result.equals(expected)) {
            System.out.println("Alles OK.");
        } else {
            System.out.println("Fehler: " + result + " != " + expected);
        }

        in = "abcdefghijklmnopqrstuvwxyz";
        expected = "abcdefghijklmnopqrstuvwxyz";
        result = Vigenere.code(in, "a");
        
        if (result.equals(expected)) {
            System.out.println("Alles OK.");
        } else {
            System.out.println("Fehler: " + result + " != " + expected);
        }
}
    
    public static void testDecode() {
        String in = "dogqsbhfqosoqsxqohfrkiunlxqoatrdwugzxjkzlec";
        String expected = "aaaneverkleinernabcdefghijklmnopqrstuvewxyz";
        String result = Vigenere.decode(in, "b");
        
        if (result.equals(expected)) {
            System.out.println("Alles OK.");
        } else {
            System.out.println("Fehler: " + result + " != " + expected);
        }

        in = "abcdefghijklmnopqrstuvwxyz";
        expected = "abcdefghijklmnopqrstuvwxyz";
        result = Vigenere.decode(in, "a");
        
        if (result.equals(expected)) {
            System.out.println("Alles OK.");
        } else {
            System.out.println("Fehler: " + result + " != " + expected);
        }
    }
    
    public static void main(String[] args) {
        testCode();
        testDecode();
    }
}
