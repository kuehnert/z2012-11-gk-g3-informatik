package test;

import crypt.ZahlenUmwandlung;

public class ZahlenUmwandlungTest {
    public static void testAlsBinaerzahl() {
        int in = 97;
        String expected = "01100001";
        String result = ZahlenUmwandlung.alsBinaerzahl(in);
        
        if (result.equals(expected)) {
            System.out.println("Alles OK.");
        } else {
            System.out.println("Fehler: " + result + " != " + expected);
        }

    }
}