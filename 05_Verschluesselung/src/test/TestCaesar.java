package test;

import crypt.Caesar;

public class TestCaesar {
    public static void testCode() {
        String in = "abcdefghijklmnopqrstuvwxyz";
        String expected = "bcdefghijklmnopqrstuvwxyza";
        String result = Caesar.code(in, "b");
        
        if (result.equals(expected)) {
            System.out.println("Alles OK.");
        } else {
            System.out.println("Fehler: " + result + " != " + expected);
        }

        in = "abcdefghijklmnopqrstuvwxyz";
        expected = "abcdefghijklmnopqrstuvwxyz";
        result = Caesar.code(in, "a");
        
        if (result.equals(expected)) {
            System.out.println("Alles OK.");
        } else {
            System.out.println("Fehler: " + result + " != " + expected);
        }
}
    
    public static void testDecode() {
        String in = "bcdefghijklmnopqrstuvwxyza";
        String expected = "abcdefghijklmnopqrstuvwxyz";
        String result = Caesar.decode(in, "b");
        
        if (result.equals(expected)) {
            System.out.println("Alles OK.");
        } else {
            System.out.println("Fehler: " + result + " != " + expected);
        }

        in = "abcdefghijklmnopqrstuvwxyz";
        expected = "abcdefghijklmnopqrstuvwxyz";
        result = Caesar.decode(in, "a");
        
        if (result.equals(expected)) {
            System.out.println("Alles OK.");
        } else {
            System.out.println("Fehler: " + result + " != " + expected);
        }
    }
    
    public static void main(String[] args) {
        testCode();
        testDecode();
    }
}
