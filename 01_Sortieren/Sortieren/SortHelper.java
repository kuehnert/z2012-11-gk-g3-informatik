import java.util.Random;
import java.util.List;
import java.util.Arrays;
import java.util.Collections;

class SortHelper {
  public static int[] best_case(int count) {
    int[] a = new int[count];
    for (int i = 0; i < count; i++) {
      a[i] = i;
    }
        
    return a;
  }
  
  public static int[] average_case(int count) {
    Random generator = new Random();
        
    int[] a = new int[count];
    for (int i = 0; i < count; i++) {
      a[i] = generator.nextInt(10 * count);
    }
        
    return a;
  }

  public static int[] worst_case(int count) {
    int[] a = new int[count];
    for (int i = 0; i < count; i++) {
      a[i] = a.length - i - 1;
    }
        
    return a;
  }
  
  public static void swap(int[] a, int i, int j) {
    int tmp = a[i];
    a[i]    = a[j];
    a[j]    = tmp;
  }

  public static void ausgeben(int[] a){
    System.out.print("[");
    for (int i = 0; i  < a.length; i++) {

      System.out.print( a[i] );
      if(i < (a.length -1)){
        System.out.print(", ");
      }
    }

    System.out.println("]");
  }
  
  public static void wertezeile(int z1, int z2, int[] a) {
    System.out.printf("%3d|%3d||", z1, z2);
    for (int i = 0; i  < a.length; i++) {
      System.out.printf( "%3d|", a[i] );
    }
    System.out.println();
  }
  
  public static void wertekopf(int[] a) {
    // Hausaufgabe
  }
}
