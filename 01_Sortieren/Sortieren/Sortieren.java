public class Sortieren {
  public void min_sort(int[] a) {
    for (int i = 0; i < a.length - 1; i++) {
      int min = i;
      for (int j = i + 1; j < a.length; j++) {
        if (a[j] < a[min]) {
          min = j;
        }
      }

      SortHelper.swap(a, i, min);
    }
  }
    
  public void bubble_sort(int[] a) {
    int tmp;
    for (int i = a.length; i > 1; i--) {
      for (int j = 0; j < i-1; j++) {
        SortHelper.wertezeile(i, j, a);
        if ( a[j] > a[j + 1] ) {
          // SortHelper.swap(a, j, j+1);
          tmp = a[j];
          a[j]    = a[j+1];
          a[j+1]    = tmp;
        }
        
      }
    }
    // for (int i = a.length-1; i > 0; i--) {
    //   for (int j = 0; j < i; j++) {
    //     if ( a[j] > a[j + 1] ) {
    //       SortHelper.swap(a, j, j+1);
    //     }
    //     
    //     // SortHelper.wertezeile(i, j, a);
    //   }
    // }
  }

  public static void insertion_sort(int[] a){
    for (int i = 1; i < a.length; i++) {
      int j = i;
      while (j > 0 && a[j] < a[j-1]) {
        SortHelper.swap(a, j, j-1);
        j = j - 1;
      }
    }  
  }  
    
  public static void zeit_messen() {
    
    try { Thread.sleep(2000); } catch (InterruptedException e) {};
		
  }
  
  public static void test_cases() {
    int[] a = SortHelper.worst_case(10);
    SortHelper.ausgeben(a);
      
    a = SortHelper.best_case(10);
    SortHelper.ausgeben(a);
  }
  
  public static void main(String[] args) {
    Sortieren sortierer = new Sortieren();
    
    for (int n = 10; n <= 10; n += 5000) {
      int[] a;
      double dauer;
      long start, ende;
      
      // Best Case
      // a = SortHelper.best_case(n);
      // start = System.nanoTime();
      // sortierer.bubble_sort(a);
      // ende  = System.nanoTime();
      // dauer = (ende - start) / 1000000000.0;
      // System.out.print(dauer);

      // average_case
      // a = SortHelper.average_case(n);
      // start = System.nanoTime();
      // sortierer.bubble_sort(a);
      // ende  = System.nanoTime();
      // dauer = (ende - start) / 1000000000.0;
      // System.out.print("\t" + dauer);

      // Worst case
      a = SortHelper.worst_case(n);
      start = System.nanoTime();
      sortierer.bubble_sort(a);
      ende  = System.nanoTime();
      dauer = (ende - start) / 1000000000.0;
      System.out.println("\t" + dauer);
    }
  }

  // public static void main(String[] args) {
  //   int[] a = SortHelper.average_case(10);
  //   SortHelper.ausgeben(a);
  // }
}
