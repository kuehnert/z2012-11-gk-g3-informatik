Voraussetzung: Unsortiertes Feld <a>

Setze <letztes> auf Länge von a - 1
Setze <tmp> auf -1

For-Schleife von 0 bis <letztes>, Zähler <start>
	Setze <min> auf <start>
	
	Setze <aktuell> auf <start> + 1
	For: <start> + 1 bis <ende>
	Wenn Element an <aktuell> < Element <min>
		....
		
	Setze <aktuell> auf <aktuell> + 1
	